// const carsInventory = require("./cars.js");

function onlyBmwAudiCars (carsInventory) {
    
    if(carsInventory === undefined || carsInventory.length === 0) {
        return [];
    }
    
    const onlyBmwAudiCarsArr = [];
    for (let i = 0; i < carsInventory.length; i++){
        // console.log(carsInventory[i]);
        if(carsInventory[i]["car_make"] === "BMW" || carsInventory[i]["car_make"] === "Audi") {
            // console.log(carsInventory[i]);
            onlyBmwAudiCarsArr.push(carsInventory[i]);
        }
    }

    console.log(onlyBmwAudiCarsArr);
    return JSON.stringify(onlyBmwAudiCarsArr);
    
}

// console.log(onlyBmwAudiCars(carsInventory));

module.exports = onlyBmwAudiCars;