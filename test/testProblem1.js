const problem1function = require("../problem1.js");

const inventory = require("../cars.js");

const result = problem1function();

// console.log(result);

console.log(`Car ${result.id} is a ${result.car_year} ${result.car_make} ${result.car_model}`);
