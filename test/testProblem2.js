const problem2function = require("../problem2.js");

const carsInventory = require("../cars.js");

const result = problem2function();

console.log(`Last car is a ${result.car_make} ${result.car_model}`);